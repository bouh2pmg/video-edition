#!/usr/bin/env python3


from sys import argv
import os
from random import choice
from shutil import copyfile
from os.path import *
from moviepy.editor import *
import moviepy.audio.fx.all as afx


# Default param#########################################################################################################
debug = False
folder = argv[1]
if folder[-1] is "/":
    folder = folder[0:-1]
name = basename(folder).split("_")[-1]
if not isfile("./video_config"):
    volumeBack = "0.035"
else:
    if isfile(folder+"/" + name + "_CONFIG"):
        file = open(folder+"/" + name + "_CONFIG", "r")
    else:
        file = open("video_config", "r")
    module = file.readline()
    module.replace("module = ", '')
    code_module = file.readline()
    code_module.replace("codeModule = ", '')
    volumeBack = file.readline()
    volumeBack = volumeBack.replace("volumeBack = ", '')
    volumeBack = float(volumeBack)
normalisation = " 0"
noisered = "0"
########################################################################################################################

# stdin parsing#########################################################################################################
for elem in argv:
    if "-d" in elem:
        debug = True
    elif ".ogg" in elem:
        file = elem
########################################################################################################################

if file is "":
    print("Invalid Audio file, please put a correct audio file as argument")
    exit(1)


# Audio cleaning########################################################################################################
file_trunc = file[:-13]
os.system("sox " + file + " " + file_trunc + "_noise.ogg" + " trim" + " 0" + " 2 " + "noiseprof "
          + file_trunc + ".noise_profile")
os.system("sox " + file + " " + file_trunc + "_clean.ogg" + " norm" + normalisation + " noisered "
          + file_trunc + ".noise_profile " + noisered)
########################################################################################################################

# Make final audio######################################################################################################
fileSourceBack = "../compositing/music/" \
                 + choice([f for f in os.listdir("../compositing/music/") if f.endswith('.mp3')])
print("Background music file: "+fileSourceBack)
copyfile(fileSourceBack, file_trunc + "_back.ogg")
empty = AudioClip(duration=8)
empty.nchannels = 1
empty.make_frame = lambda t: 0
clean = AudioFileClip(file_trunc+"_clean.ogg")
durationTot = 8 + clean.duration + 2
back = AudioFileClip(file_trunc+"_back.ogg")\
    .fx(afx.volumex, float(volumeBack))
finalAudio = concatenate_audioclips([empty, clean])
ratio = 1 + int(durationTot/back.duration)
finalAudio = CompositeAudioClip([concatenate_audioclips([back] * ratio), finalAudio]) \
    .set_duration(durationTot) \
    .audio_fadein(3) \
    .audio_fadeout(3)
finalAudio.write_audiofile(file_trunc+".mp3")
########################################################################################################################

# Deleting all the debugging files######################################################################################
if debug is False:
    print("Deleting all the debug file...")
    os.system("rm "+file_trunc+"_back.ogg "+file_trunc+"_noise.ogg "+file_trunc+".noise_profile")
print("### Audio is clean ###")
########################################################################################################################
