#!/usr/bin/env python3

from numpy import cos
from numpy import exp
from moviepy.editor import *
from sys import argv
from os.path import *
import os
import imageio
imageio.plugins.ffmpeg.download()


folder = argv[1]
if folder[-1] is "/":
    folder = folder[0:-1]
name = basename(folder).split("_")[-1]
if not isfile("./video_config"):
    module = "B- XX"
    code_module = "B-XX-XX"
else:
    if isfile(folder+"/" + name + "_CONFIG"):
        file = open(folder+"/" + name + "_CONFIG", "r")
    else:
        file = open("video_config", "r")
    module = file.readline()
    module = module.replace("module = ", '')
    code_module = file.readline()
    code_module = code_module.replace("codeModule = ", '')
videoCodec = 'libx264'
audioCodec = 'aac'


# Verifying the font####################################################################################################
if os.path.exists("/usr/share/fonts/Ubuntu/Ubuntu-Bold.ttf"):
    titleFont = "Ubuntu-Bold"
else:
    print("[ERROR] Font Ubuntu-Bold not found...")
    exit(1)
########################################################################################################################


# Compositing intro#####################################################################################################
def bounce(t: int, pos: int, freq: int, decay: int, start: float, duration: float = 1.5) -> float:
    if t < start:
        return -100
    if t >= start + duration:
        return pos
    ratio = (t - start) / duration
    return pos * (1 - cos((freq + 0.5) * ratio * 3.14159) / (exp(decay * ratio) ** 2))


video = VideoFileClip(folder+"/"+name+"_ORIGINAL.mp4")
width = video.w
height = video.h
fps = video.fps
durationMainVideo = video.duration

print("[Compositing] Title preparation")
durationTitle = 4

titleText1 = TextClip(name,
                      fontsize=65,
                      font=titleFont,
                      color='white',
                      kerning=10,
                      stroke_width=5) \
    .set_position(lambda t: ('center', bounce(t, 0.23 * height, 5, 6, 0.8)))

titleText2 = TextClip(module,
                      fontsize=52,
                      font=titleFont,
                      color='gray') \
    .set_position(lambda t: ('center', bounce(t, 0.32 * height, 5, 9, 0.5)))

titleText3 = TextClip(code_module,
                      fontsize=30,
                      font=titleFont,
                      color='white') \
    .set_position(lambda t: ('center', bounce(t, 0.60 * height, 5, 9, 0)))

titleText4 = TextClip('E P I T E C H',
                      fontsize=25,
                      font=titleFont,
                      color='gray') \
    .set_position(lambda t: ('center', bounce(t, 0.65 * height, 5, 9, 0)))

titleScreen = CompositeVideoClip([titleText1, titleText2, titleText3, titleText4], size=(width, height)) \
    .set_duration(durationTitle) \
    .set_fps(fps) \
    .fadeout(0.5)
########################################################################################################################

# Drop Sequence composition#############################################################################################
print("[Compositing] Drop sequences generation")
duration1 = 0.1
nbFrames2 = 10


drop = ImageClip("../compositing/images/logo_goutte.png", transparent=True) \
    .set_duration(duration1) \
    .resize(width=lambda t: ((1 - t / duration1) + 0.07) * width) \
    .set_position(('center', 'center'))

drop1 = CompositeVideoClip([drop], size=(width, height))

drop = ImageClip("../compositing/images/logo_goutte_600.png", transparent=True) \
    .set_duration(durationTitle - duration1 - (nbFrames2 - 1) / fps) \
    .resize(width=0.07 * width) \
    .set_position(('center', 'center'))

drop2 = CompositeVideoClip([drop], size=(width, height))
dropAnimTitle = concatenate_videoclips([drop1, drop2])

for i in range(1, nbFrames2):
    drop = ImageClip("../compositing/images/logo_goutte_600.png", transparent=True) \
        .set_duration(1 / fps) \
        .set_opacity(1 - 0.20 * i / nbFrames2) \
        .resize(width=(0.06 + 0.04 * i * i / (nbFrames2 * nbFrames2)) * width) \
        .set_position(('center', 'center'))

    drop = CompositeVideoClip([drop], size=(width, height))
    dropAnimTitle = concatenate_videoclips([dropAnimTitle, drop])

titleScreen = CompositeVideoClip([dropAnimTitle, titleScreen])

drop = ImageClip("../compositing/images/logo_goutte_600.png", transparent=True) \
    .set_duration(1 / fps) \
    .set_opacity(0.80) \
    .resize(width=0.10 * width) \
    .set_position(('center', 'center'))
dropAnimVideo = CompositeVideoClip([drop], size=(width, height))

nbFrames3 = 3 * nbFrames2
for i in range(1, nbFrames3 + 1):
    drop = ImageClip("../compositing/images/logo_goutte_600.png", transparent=True) \
        .set_duration(1 / fps) \
        .set_opacity(0.80 - 0.20 * i / nbFrames2) \
        .resize(width=(0.10 + 0.04 * (i / nbFrames2)) * width) \
        .set_position(('center', 'center'))
    drop = CompositeVideoClip([drop], size=(width, height))
    dropAnimVideo = concatenate_videoclips([dropAnimVideo, drop])

video = CompositeVideoClip([video, dropAnimVideo])

drop = ImageClip("../compositing/images/logo_goutte.png", transparent=True) \
    .set_duration(durationMainVideo - dropAnimVideo.duration) \
    .set_opacity(0.15) \
    .resize(width=0.22 * width) \
    .set_position(('center', 'center')) \
    .fadeout(3)
########################################################################################################################

# Write final title video###############################################################################################
titleScreen.write_videofile("title.mp4", fps=25, codec=videoCodec, audio_codec=audioCodec,
                            temp_audiofile="temp-"+str(name)+".m4a", remove_temp=True)
os.system("mv title.mp4 "+folder)
########################################################################################################################