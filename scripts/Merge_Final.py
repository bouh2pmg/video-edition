#!/usr/bin/env python3

import asstosrt
import re
from sys import argv
from os.path import *
from os import system
from shutil import copyfile
from moviepy.editor import *

folder = argv[1]
if folder[-1] is "/":
    folder = folder[0:-1]
name = basename(folder).split("_")[-1]
if not isfile("./video_config"):
    print("[ERROR] No config file found...")
    module = "B- XX"
    code_module = "B-XX-XX_"
else:
    if isfile(folder+"/" + name + "_CONFIG"):
        file = open(folder+"/" + name + "_CONFIG", "r")
    else:
        file = open("video_config", "r")
    title = file.readline()    
    module = file.readline()
    module = module.replace("module = ", '')
    module = module.replace("\n", '')
    code_module = file.readline()
    code_module = code_module.replace("codeModule = ", '')
    code_module = code_module.replace("\n", '')
    code_module += '_'
fileSubFinal = code_module+str(name) + ".srt"

# Delay utils function##################################################################################################


def delay(h:int, m:int, s:int, dec:int) -> str:
    newS = (s+dec)%60
    m2 = m+(s+dec)//60
    newM = m2%60
    newH = h+m2//60
    return ("0" if newH<10 else "")+str(newH)+":"+("0" if newM<10 else "")\
           +str(newM)+":"+("0" if newS<10 else "")+str(newS)


########################################################################################################################

# Verifying al the file and recompute if necessary######################################################################
if not isfile(folder+"/"+str(name)+"_ORIGINAL.mp4"):
    print("[ERROR] Original video file not found")
    exit(1)
else:
    print("[INFO] Verifying vidéo format and codec...")
    original = VideoFileClip(folder+"/"+str(name)+"_ORIGINAL.mp4")
    if not original.w == 1280 or not original.h == 720:
        print("[ERROR] Video resolution is not 1280x720, please recompute the video")
        exit(1)
    elif not original.fps == 25:
        print("[ERROR] invalid fps please recompute in 25 fps")
        exit(1)
    codec = "libx264"
    if not isfile(folder+"/"+str(name)+"_FINAL.mp4"):
        original.write_videofile(str(name)+"_NEW.mp4", fps=25, codec=codec, audio=False,
                            temp_audiofile="temp-"+str(name)+".m4a", remove_temp=True)

if not isfile(folder+"/"+str(name)+".mp3"):
    print("[WARNING] Clean audio file not found")
    print("[INFO] Cleaning sound")
    system("./clean_audio.py "+folder+"/"+str(name)+"_ORIGINAL.ogg")

if not isfile(folder+"/"+"title.mp4"):
    print("[WARNING] title not found")
    print("[INFO] Computing title")
    system("./compute_title.py "+folder+"/")

title_video = folder+"/"+"title.mp4 "
video = str(name)+"_NEW.mp4 "
########################################################################################################################

# Merging Video#########################################################################################################
if not isfile(folder+"/"+str(name)+"_FINAL.mp4"):
    system("mencoder -ovc copy -oac copy "+"../compositing/vidéo/outro.mp4 " + title_video
           + video + "../compositing/vidéo/outro.mp4" +" -o "+ str(name)+"_FINAL.mp4")
########################################################################################################################

    system("mv " + str(name)+"_FINAL.mp4" + " " + folder)
    system("rm " + str(name)+"_NEW.mp4")

# Merging with audio & subtitle#########################################################################################
audio = AudioFileClip(folder+"/"+str(name)+".mp3")
video = VideoFileClip(folder+"/"+str(name)+"_FINAL.mp4")
video.audio = audio
video.write_videofile(code_module+str(name) + ".mp4", fps=25, codec='libx264',
                      audio_codec='aac', temp_audiofile="temp-"+str(name)+".m4a", remove_temp=True)
system("mv "+code_module + str(name) + ".mp4 " + folder)
########################################################################################################################

# Subtitles ############################################################################################################
# ATT: requires asstosrt python module'
if not isfile(folder+"/"+name+"_ORIGINAL.srt") or isfile(folder+"/"+name+"_ORIGINAL.ass"):
    print("[WARNING] No subtitle found...")
else:
    if isfile(folder+"/"+name+"_ORIGINAL.ass"):
        fileSubOriginal = folder + "/" + name + "_ORIGINAL.ass"
        ass_file = open(folder+"/"+name+"_ORIGINAL.ass")
        new_content = asstosrt.convert(ass_file)
        new_file = open(fileSubOriginal.replace("ass", "srt"), "a")
        new_file.write(new_content)
        fileSubFinal = fileSubOriginal.replace(".ass", ".srt")
    else:
        fileSubOriginal = folder + "/" + name + "_ORIGINAL.srt"
        copyfile(fileSubOriginal, fileSubFinal)

    newFile = []
    delayTime = int((10 * (8 + float(0)) + 5) // 10)  # hand rounding
    print("subtitles delay: " + str(delayTime) + "s")
    with open(fileSubFinal, 'r') as f:
        lines = f.readlines()
    for line in lines:
        p = re.compile("^(\d\d):(\d\d):(\d\d),(\d\d\d) --> (\d\d):(\d\d):(\d\d),(\d\d\d)\n")
        time = p.split(line)
        if len(time) == 10:
            if fileSubOriginal[-4:] == ".ass":  # !BUG : décalge d'un chiffre dans les millisecondes
                newFile.append(
                    delay(int(time[1]), int(time[2]), int(time[3]), delayTime) + "," + time[4][-2:] + "0 --> " + delay(
                        int(time[5]), int(time[6]), int(time[7]), delayTime) + "," + time[8][-2:] + "0\n")
            else:
                newFile.append(
                    delay(int(time[1]), int(time[2]), int(time[3]), delayTime) + "," + time[4] + " --> " + delay(
                        int(time[5]), int(time[6]), int(time[7]), delayTime) + "," + time[8] + "\n")
        else:
            newFile.append(line)

    with open(fileSubFinal, 'w') as f:
        f.seek(0)
        f.writelines(newFile)
    system("mv "+fileSubFinal + " " + folder)
########################################################################################################################
