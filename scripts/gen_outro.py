#!/usr/bin/env python3

from moviepy.editor import *

if __name__ == '__main__':

    width = 1280
    height = 720
    durationLogo = 4
    fps = 25
    videoCodec = 'libx264'

    background = ColorClip((width, height), col=(66, 169, 224)) \
        .set_duration(0.8) \
        .fadein(0.35) \
        .fadeout(0.3)

    logo = ImageClip("../compositing/images/logo-epitech-pure_1500.png") \
        .resize(width=0.7 * width) \
        .set_position(('center', 'center'))

    logoAnim = CompositeVideoClip([background.set_start(1.2), logo], size=(width, height)) \
        .set_duration(durationLogo) \
        .fadein(0.5) \
        .fadeout(1) \
        .set_fps(fps)

    logoAnim.write_videofile("outro.mp4", fps=25, codec=videoCodec)
